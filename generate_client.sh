#!/usr/bin/env bash
set -e

OPENAPI_DOCKER_IMAGE_VER="v5.2.0"

# https://stackoverflow.com/questions/59895/how-to-get-the-source-directory-of-a-bash-script-from-within-the-script-itself
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

if [ $# -lt 3 ] ; then
  echo "Usage: $0 <input_file.{yaml,json}> <output_crate_dir> <package_version>"
  exit 1
fi


HOST_OUT_CRATE_DIR="$2"

# Take API Definition and generate client
OPENAPI_IN_FILE=/local/$(realpath --relative-to="$DIR" "$1")
OUT_CRATE_DIR=/local/$(realpath --relative-to="$DIR" "$HOST_OUT_CRATE_DIR")
PACKAGE_NAME=$(basename "$HOST_OUT_CRATE_DIR")
VERSION="$3"

docker run \
  --rm \
  --user $(id -u):$(id -g) \
  -v ${DIR}:/local openapitools/openapi-generator-cli:$OPENAPI_DOCKER_IMAGE_VER\
  generate \
  -g rust \
  --library reqwest \
  --additional-properties=packageName=$PACKAGE_NAME,packageVersion=$VERSION \
  -i $OPENAPI_IN_FILE \
  -o $OUT_CRATE_DIR

cargo fmt --manifest-path "$HOST_OUT_CRATE_DIR/Cargo.toml"
