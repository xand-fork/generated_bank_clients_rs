GENERATE_CLIENT_SCRIPT = ./generate_client.sh

MCB_ACCT_YAML = ./mcb_inputs/AcctSvc.json
MCB_ACCT_CRATE = ./mcb/mcb_acct_gen/
MCB_ACCT_VER = 5.0.0

MCB_ACCT_TRN_YAML = ./mcb_inputs/AcctTrnSvc.json
MCB_ACCT_TRN_CRATE = ./mcb/mcb_acct_trn_gen/
MCB_ACCT_TRN_VER = 5.0.0

MCB_AUTH_YAML = ./mcb_inputs/AccessTokenGenerator.json
MCB_AUTH_CRATE = ./mcb/mcb_auth_gen/
MCB_AUTH_VER = 5.0.0

MCB_TRANSFER_YAML = ./mcb_inputs/XferSvc.json
MCB_TRANSFER_CRATE = ./mcb/mcb_transfer_gen/
MCB_TRANSFER_VER = 5.0.0

TREASURY_PRIME_YAML = ./treasury_prime/treasury_api.yaml
TREASURY_PRIME_CRATE = ./treasury_prime/treasury_prime_client/
TREASURY_PRIME_VER = 4.0.0

all: generate_treasury_prime generate_mcb

# all MCB targets
generate_mcb: generate_mcb_acct generate_mcb_acct_trn generate_mcb_auth generate_mcb_transfer

# MCB account
generate_mcb_acct: $(GENERATE_CLIENT_SCRIPT) $(MCB_ACCT_YAML)
	rm -rf $(MCB_ACCT_CRATE)
	$(GENERATE_CLIENT_SCRIPT) $(MCB_ACCT_YAML) $(MCB_ACCT_CRATE) $(MCB_ACCT_VER)
	cd $(MCB_ACCT_CRATE) && cargo build


# MCB account "trn" (transfer? transaction?)
generate_mcb_acct_trn: $(GENERATE_CLIENT_SCRIPT) $(MCB_ACCT_TRN_YAML)
	rm -rf $(MCB_ACCT_TRN_CRATE)
	$(GENERATE_CLIENT_SCRIPT) $(MCB_ACCT_TRN_YAML) $(MCB_ACCT_TRN_CRATE) $(MCB_ACCT_TRN_VER)
	cd $(MCB_ACCT_TRN_CRATE) && cargo build


# MCB auth
generate_mcb_auth: $(GENERATE_CLIENT_SCRIPT) $(MCB_AUTH_YAML)
	rm -rf $(MCB_AUTH_CRATE)
	$(GENERATE_CLIENT_SCRIPT) $(MCB_AUTH_YAML) $(MCB_AUTH_CRATE) $(MCB_AUTH_VER)
	cd $(MCB_AUTH_CRATE) && cargo build


# MCB transfer
generate_mcb_transfer: $(GENERATE_CLIENT_SCRIPT) $(MCB_TRANSFER_YAML)
	rm -rf $(MCB_TRANSFER_CRATE)
	$(GENERATE_CLIENT_SCRIPT) $(MCB_TRANSFER_YAML) $(MCB_TRANSFER_CRATE) $(MCB_TRANSFER_VER)
	cd $(MCB_TRANSFER_CRATE) && cargo build


# Treasury Prime
generate_treasury_prime: $(GENERATE_CLIENT_SCRIPT) $(TREASURY_PRIME_YAML)
	rm -rf $(TREASURY_PRIME_CRATE)
	$(GENERATE_CLIENT_SCRIPT) $(TREASURY_PRIME_YAML) $(TREASURY_PRIME_CRATE) $(TREASURY_PRIME_VER)
	cd $(TREASURY_PRIME_CRATE) && cargo build
