# \AccountApi

All URIs are relative to *http://localhost:3000/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**account**](AccountApi.md#account) | **GET** /account/{id} | 
[**account_summaries_by_account_number**](AccountApi.md#account_summaries_by_account_number) | **GET** /account | 



## account

> crate::models::AccountDetail account(id)


Retrieves account info for given account id.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **String** | Unique account id | [required] |

### Return type

[**crate::models::AccountDetail**](AccountDetail.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## account_summaries_by_account_number

> crate::models::AccountSummaries account_summaries_by_account_number(account_number)


 Retrieves account summaries for given account_number, using a filter query.  Returns an array. The API spec validation constrains responses to only one item. 

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**account_number** | **String** | The bank account_number used by the customer. | [required] |

### Return type

[**crate::models::AccountSummaries**](AccountSummaries.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

