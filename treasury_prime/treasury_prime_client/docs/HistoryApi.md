# \HistoryApi

All URIs are relative to *http://localhost:3000/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_book_transfers**](HistoryApi.md#get_book_transfers) | **GET** /book | 



## get_book_transfers

> crate::models::BookTransfers get_book_transfers(from_date, to_date, page_cursor)


Retrieves book tranfers in reverse order of their create date. (Most recently created are returned first)

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**from_date** | **String** |  | [required] |
**to_date** | **String** |  | [required] |
**page_cursor** | Option<**String**> | The start of the next page will be right after this book transfer id |  |

### Return type

[**crate::models::BookTransfers**](BookTransfers.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

