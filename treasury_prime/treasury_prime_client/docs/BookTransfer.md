# BookTransfer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**amount** | **String** |  | 
**from_account_id** | **String** |  | 
**status** | **String** |  | 
**to_account_id** | **String** |  | 
**userdata** | Option<[**crate::models::BookTransferUserdata**](BookTransfer_userdata.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


