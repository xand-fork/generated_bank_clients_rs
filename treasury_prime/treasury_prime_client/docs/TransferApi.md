# \TransferApi

All URIs are relative to *http://localhost:3000/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_book_transfer_request**](TransferApi.md#create_book_transfer_request) | **POST** /book | 



## create_book_transfer_request

> crate::models::BookTransfer create_book_transfer_request(book_transfer_request)


Makes a book transfer request given an amount, to_account_id, from_account_id, userdata to correlate the transaction, and an optional description. Amounts are accurate to two decimal places. Receives the book transfer object as a response, regardless of its status.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**book_transfer_request** | [**BookTransferRequest**](BookTransferRequest.md) |  | [required] |

### Return type

[**crate::models::BookTransfer**](BookTransfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

