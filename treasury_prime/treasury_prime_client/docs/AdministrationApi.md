# \AdministrationApi

All URIs are relative to *http://localhost:3000/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**home**](AdministrationApi.md#home) | **GET** / | 



## home

> crate::models::ApiVersionInfo home()


Home page for the API

### Parameters

This endpoint does not need any parameter.

### Return type

[**crate::models::ApiVersionInfo**](APIVersionInfo.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

