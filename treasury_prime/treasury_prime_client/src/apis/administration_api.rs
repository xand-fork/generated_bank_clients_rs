/*
 * Member API
 *
 * Member API routes and contracts.
 *
 * The version of the OpenAPI document: 1.0.0
 *
 * Generated by: https://openapi-generator.tech
 */

use reqwest;

use super::{configuration, Error};
use crate::apis::ResponseContent;

/// struct for typed errors of method `home`
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum HomeError {
    Status403(crate::models::InlineResponse403),
    Status404(crate::models::InlineResponse404),
    UnknownValue(serde_json::Value),
}

/// Home page for the API
pub async fn home(
    configuration: &configuration::Configuration,
) -> Result<crate::models::ApiVersionInfo, Error<HomeError>> {
    let local_var_client = &configuration.client;

    let local_var_uri_str = format!("{}/", configuration.base_path);
    let mut local_var_req_builder =
        local_var_client.request(reqwest::Method::GET, local_var_uri_str.as_str());

    if let Some(ref local_var_user_agent) = configuration.user_agent {
        local_var_req_builder =
            local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<HomeError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent {
            status: local_var_status,
            content: local_var_content,
            entity: local_var_entity,
        };
        Err(Error::ResponseError(local_var_error))
    }
}
