/*
 * Member API
 *
 * Member API routes and contracts.
 *
 * The version of the OpenAPI document: 1.0.0
 *
 * Generated by: https://openapi-generator.tech
 */

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct ApiVersionInfo {
    #[serde(rename = "api_version", skip_serializing_if = "Option::is_none")]
    pub api_version: Option<String>,
    #[serde(rename = "version", skip_serializing_if = "Option::is_none")]
    pub version: Option<String>,
    #[serde(rename = "time", skip_serializing_if = "Option::is_none")]
    pub time: Option<String>,
}

impl ApiVersionInfo {
    pub fn new() -> ApiVersionInfo {
        ApiVersionInfo {
            api_version: None,
            version: None,
            time: None,
        }
    }
}
