/*
 * Member API
 *
 * Member API routes and contracts.
 *
 * The version of the OpenAPI document: 1.0.0
 *
 * Generated by: https://openapi-generator.tech
 */

/// BookTransfers : Array of book transfer objects, with optional cursor pagination. Object may come back without userdata if created outside of XAND processes.

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct BookTransfers {
    #[serde(rename = "data")]
    pub data: Vec<crate::models::BookTransfer>,
    #[serde(rename = "page_next", skip_serializing_if = "Option::is_none")]
    pub page_next: Option<String>,
}

impl BookTransfers {
    /// Array of book transfer objects, with optional cursor pagination. Object may come back without userdata if created outside of XAND processes.
    pub fn new(data: Vec<crate::models::BookTransfer>) -> BookTransfers {
        BookTransfers {
            data,
            page_next: None,
        }
    }
}
