/*
 * Member API
 *
 * Member API routes and contracts.
 *
 * The version of the OpenAPI document: 1.0.0
 *
 * Generated by: https://openapi-generator.tech
 */

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct BookTransfer {
    #[serde(rename = "id")]
    pub id: String,
    #[serde(rename = "amount")]
    pub amount: String,
    #[serde(rename = "from_account_id")]
    pub from_account_id: String,
    #[serde(rename = "status")]
    pub status: String,
    #[serde(rename = "to_account_id")]
    pub to_account_id: String,
    #[serde(rename = "userdata", skip_serializing_if = "Option::is_none")]
    pub userdata: Option<Box<crate::models::BookTransferUserdata>>,
}

impl BookTransfer {
    pub fn new(
        id: String,
        amount: String,
        from_account_id: String,
        status: String,
        to_account_id: String,
    ) -> BookTransfer {
        BookTransfer {
            id,
            amount,
            from_account_id,
            status,
            to_account_id,
            userdata: None,
        }
    }
}
