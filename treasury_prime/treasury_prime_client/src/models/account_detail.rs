/*
 * Member API
 *
 * Member API routes and contracts.
 *
 * The version of the OpenAPI document: 1.0.0
 *
 * Generated by: https://openapi-generator.tech
 */

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct AccountDetail {
    #[serde(rename = "available_balance")]
    pub available_balance: String,
    #[serde(rename = "current_balance")]
    pub current_balance: String,
}

impl AccountDetail {
    pub fn new(available_balance: String, current_balance: String) -> AccountDetail {
        AccountDetail {
            available_balance,
            current_balance,
        }
    }
}
