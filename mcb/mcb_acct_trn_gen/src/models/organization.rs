/*
 * IFX Services for Metropolitan
 *
 * IFX Services for Metropolitan
 *
 * The version of the OpenAPI document: 0.0.1
 *
 * Generated by: https://openapi-generator.tech
 */

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Organization {
    #[serde(rename = "OrgId", skip_serializing_if = "Option::is_none")]
    pub org_id: Option<String>,
}

impl Organization {
    pub fn new() -> Organization {
        Organization { org_id: None }
    }
}
