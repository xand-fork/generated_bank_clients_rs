/*
 * IFX Services for Metropolitan
 *
 * IFX Services for Metropolitan
 *
 * The version of the OpenAPI document: 0.0.1
 *
 * Generated by: https://openapi-generator.tech
 */

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Client {
    #[serde(rename = "Organization", skip_serializing_if = "Option::is_none")]
    pub organization: Option<Box<crate::models::Organization>>,
    #[serde(rename = "ClientAppIdent", skip_serializing_if = "Option::is_none")]
    pub client_app_ident: Option<String>,
}

impl Client {
    pub fn new() -> Client {
        Client {
            organization: None,
            client_app_ident: None,
        }
    }
}
