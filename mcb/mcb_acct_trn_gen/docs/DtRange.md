# DtRange

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**to_date** | Option<**String**> |  | [optional]
**from_date** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


