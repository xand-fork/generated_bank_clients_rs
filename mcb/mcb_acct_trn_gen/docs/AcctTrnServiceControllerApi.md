# \AcctTrnServiceControllerApi

All URIs are relative to *http://localhost:8091/efxService/v0_1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**acct_trn_inq_using_post**](AcctTrnServiceControllerApi.md#acct_trn_inq_using_post) | **POST** /AcctTrn | acctTrnInq



## acct_trn_inq_using_post

> crate::models::AcctTrnInqRs acct_trn_inq_using_post(acct_trn_inq_rq)
acctTrnInq

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**acct_trn_inq_rq** | [**AcctTrnInqRq**](AcctTrnInqRq.md) | AcctTrnInqRq | [required] |

### Return type

[**crate::models::AcctTrnInqRs**](AcctTrnInqRs.md)

### Authorization

[mcb_auth](../README.md#mcb_auth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

