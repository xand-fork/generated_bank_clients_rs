# AcctTrnInqRsAcctTrnInqRs

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**acct_trn_rec** | Option<[**Vec<crate::models::AcctTrnRec>**](AcctTrnRec.md)> |  | [optional]
**efx_hdr** | Option<[**crate::models::EfxHdr**](EFXHdr.md)> |  | [optional]
**efx_status** | Option<[**crate::models::Status**](Status.md)> |  | [optional]
**rec_ctrl_out** | Option<[**crate::models::RecCtrlOut**](RecCtrlOut.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


