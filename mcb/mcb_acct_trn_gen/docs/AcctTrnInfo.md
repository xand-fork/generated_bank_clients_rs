# AcctTrnInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**acct_ref** | Option<[**crate::models::AcctRef**](AcctRef.md)> |  | [optional]
**branch_ident** | Option<**String**> |  | [optional]
**desc** | Option<**String**> |  | [optional]
**dr_cr_type** | Option<**String**> |  | [optional]
**eff_dt** | Option<**String**> |  | [optional]
**external_trn_code** | Option<**String**> |  | [optional]
**posted_dt** | Option<**String**> |  | [optional]
**stmt_running_bal** | Option<[**Vec<crate::models::StmtRunningBal>**](StmtRunningBal.md)> |  | [optional]
**teller_ident** | Option<**String**> |  | [optional]
**trn_amt** | Option<[**crate::models::TrnAmt**](TrnAmt.md)> |  | [optional]
**trn_code** | Option<**String**> |  | [optional]
**trn_dt** | Option<**String**> |  | [optional]
**trn_rev_type** | Option<**String**> |  | [optional]
**trn_src** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


