# EfxHdr

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client** | Option<[**crate::models::Client**](Client.md)> |  | [optional]
**tracking** | Option<[**crate::models::Tracking**](Tracking.md)> |  | [optional]
**version** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


