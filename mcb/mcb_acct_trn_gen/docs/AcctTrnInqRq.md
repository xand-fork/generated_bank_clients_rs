# AcctTrnInqRq

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**acct_trn_inq_rq** | Option<[**crate::models::AcctTrnInqRqAcctTrnInqRq**](AcctTrnInqRq_AcctTrnInqRq.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


