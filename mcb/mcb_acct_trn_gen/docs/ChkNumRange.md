# ChkNumRange

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**chk_num_end** | Option<**String**> |  | [optional]
**chk_num_start** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


