# RelationshipMgr

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**desc** | Option<**String**> |  | [optional]
**relationship_mgr_ident** | Option<**String**> |  | [optional]
**relationship_role** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


