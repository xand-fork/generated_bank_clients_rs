# RelationshipPricingAcctData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**relationship_pricing_rate_opt** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


