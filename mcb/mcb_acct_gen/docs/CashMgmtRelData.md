# CashMgmtRelData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cash_mgmt_rel_type** | Option<**String**> |  | [optional]
**sweep_acct_keys** | Option<[**crate::models::SweepAcctKeys**](SweepAcctKeys.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


