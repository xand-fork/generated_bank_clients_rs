# Tracking

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**originating_trn_id** | Option<**String**> |  | [optional]
**parent_trn_id** | Option<**String**> |  | [optional]
**trn_id** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


