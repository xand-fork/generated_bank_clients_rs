# RecurRule

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recur_interval** | Option<**i64**> |  | [optional]
**recur_type** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


