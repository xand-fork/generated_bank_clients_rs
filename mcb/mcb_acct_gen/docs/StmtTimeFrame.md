# StmtTimeFrame

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recur_rule** | Option<[**Vec<crate::models::RecurRule>**](RecurRule.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


