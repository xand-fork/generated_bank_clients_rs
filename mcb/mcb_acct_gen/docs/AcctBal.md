# AcctBal

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bal_type** | Option<[**crate::models::BalType**](BalType.md)> |  | [optional]
**cur_amt** | Option<[**crate::models::CurAmt**](CurAmt.md)> |  | [optional]
**desc** | Option<**String**> |  | [optional]
**exp_dt** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


