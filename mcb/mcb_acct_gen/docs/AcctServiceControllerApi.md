# \AcctServiceControllerApi

All URIs are relative to *http://localhost:8091*

Method | HTTP request | Description
------------- | ------------- | -------------
[**account_inquiry_using_post**](AcctServiceControllerApi.md#account_inquiry_using_post) | **POST** /Accounts | accountInquiry



## account_inquiry_using_post

> crate::models::AcctInqRs account_inquiry_using_post(acct_inq_rq)
accountInquiry

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**acct_inq_rq** | [**AcctInqRq**](AcctInqRq.md) | AcctInqRq | [required] |

### Return type

[**crate::models::AcctInqRs**](AcctInqRs.md)

### Authorization

[mcb_auth](../README.md#mcb_auth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

