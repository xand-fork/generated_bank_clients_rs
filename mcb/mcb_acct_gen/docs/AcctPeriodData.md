# AcctPeriodData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**acct_amt_type** | Option<**String**> |  | [optional]
**acct_period_type** | Option<**String**> |  | [optional]
**amt** | Option<**String**> |  | [optional]
**count** | Option<**String**> |  | [optional]
**eff_dt** | Option<**String**> |  | [optional]
**exp_dt** | Option<**String**> |  | [optional]
**last_occur_ind** | Option<**bool**> |  | [optional]
**last_occurance_dt** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


