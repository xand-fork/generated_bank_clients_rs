# AcctInqRq

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**acct_inq_rq** | Option<[**crate::models::AcctInqRqAcctInqRq**](AcctInqRq_AcctInqRq.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


