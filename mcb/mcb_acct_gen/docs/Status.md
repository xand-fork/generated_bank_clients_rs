# Status

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ns1_severity** | Option<**String**> |  | [optional]
**ns1_status_code** | Option<**String**> |  | [optional]
**ns1_status_desc** | Option<**String**> |  | [optional]
**ns1_svc_provider_name** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


