# SvcChgMethod

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**svc_chg_method_opt** | Option<**String**> |  | [optional]
**svc_chg_method_type** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


