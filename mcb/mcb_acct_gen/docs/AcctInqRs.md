# AcctInqRs

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**acct_inq_rs** | Option<[**crate::models::AcctInqRsAcctInqRs**](AcctInqRs_AcctInqRs.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


