# AcctInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**acct_bal** | Option<[**Vec<crate::models::AcctBal>**](AcctBal.md)> |  | [optional]
**acct_dtl_status** | Option<**String**> |  | [optional]
**acct_memo_data** | Option<[**Vec<crate::models::AcctMemoData>**](AcctMemoData.md)> |  | [optional]
**acct_period_data** | Option<[**Vec<crate::models::AcctPeriodData>**](AcctPeriodData.md)> |  | [optional]
**acct_stmt_data** | Option<[**crate::models::AcctStmtData**](AcctStmtData.md)> |  | [optional]
**acct_title_option** | Option<**String**> |  | [optional]
**acct_type** | Option<[**crate::models::AcctType**](AcctType.md)> |  | [optional]
**acct_use** | Option<**String**> |  | [optional]
**cash_mgmt_rel_data** | Option<[**crate::models::CashMgmtRelData**](CashMgmtRelData.md)> |  | [optional]
**date_data** | Option<[**Vec<crate::models::DateData>**](DateData.md)> |  | [optional]
**dep_acct_data** | Option<[**crate::models::DepAcctData**](DepAcctData.md)> |  | [optional]
**desc** | Option<**String**> |  | [optional]
**doc_distribution_option** | Option<**String**> |  | [optional]
**employee_ind** | Option<**String**> |  | [optional]
**fee** | Option<[**Vec<crate::models::Fee>**](Fee.md)> |  | [optional]
**open_dt** | Option<**String**> |  | [optional]
**originating_branch** | Option<**String**> |  | [optional]
**overdraft_data** | Option<[**crate::models::OverdraftData**](OverdraftData.md)> |  | [optional]
**product_ident** | Option<**String**> |  | [optional]
**rate** | Option<**String**> |  | [optional]
**relationship_mgr** | Option<[**Vec<crate::models::RelationshipMgr>**](RelationshipMgr.md)> |  | [optional]
**relationship_pricing_acct_data** | Option<[**crate::models::RelationshipPricingAcctData**](RelationshipPricingAcctData.md)> |  | [optional]
**svc_chg_data** | Option<[**crate::models::SvcChgData**](SvcChgData.md)> |  | [optional]
**tax_incentive_type** | Option<**String**> |  | [optional]
**trn_restriction** | Option<**String**> |  | [optional]
**withholding_option** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


