# AcctInqRsAcctInqRs

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**acct_rec** | Option<[**crate::models::AcctRec**](AcctRec.md)> |  | [optional]
**efx_hdr** | Option<[**crate::models::EfxHdr**](EFXHdr.md)> |  | [optional]
**efx_status** | Option<[**crate::models::Status**](Status.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


