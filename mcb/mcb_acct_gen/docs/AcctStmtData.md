# AcctStmtData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**combined_stmt_code** | Option<**String**> |  | [optional]
**combined_stmt_ident** | Option<**String**> |  | [optional]
**last_stmt_dt** | Option<**String**> |  | [optional]
**stmt_time_frame** | Option<[**crate::models::StmtTimeFrame**](StmtTimeFrame.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


