# SvcChgFlatFee

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**svc_chg_flat_fee_amt** | Option<[**Vec<crate::models::SvcChgFlatFeeAmt>**](SvcChgFlatFeeAmt.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


