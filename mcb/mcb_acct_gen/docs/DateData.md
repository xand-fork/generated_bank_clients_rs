# DateData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date_type** | Option<**String**> |  | [optional]
**date_value** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


