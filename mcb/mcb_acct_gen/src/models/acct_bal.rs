/*
 * IFX Services for Metropolitan
 *
 * IFX Services for Metropolitan
 *
 * The version of the OpenAPI document: 0.0.1
 *
 * Generated by: https://openapi-generator.tech
 */

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct AcctBal {
    #[serde(rename = "BalType", skip_serializing_if = "Option::is_none")]
    pub bal_type: Option<Box<crate::models::BalType>>,
    #[serde(rename = "CurAmt", skip_serializing_if = "Option::is_none")]
    pub cur_amt: Option<Box<crate::models::CurAmt>>,
    #[serde(rename = "Desc", skip_serializing_if = "Option::is_none")]
    pub desc: Option<String>,
    #[serde(rename = "ExpDt", skip_serializing_if = "Option::is_none")]
    pub exp_dt: Option<String>,
}

impl AcctBal {
    pub fn new() -> AcctBal {
        AcctBal {
            bal_type: None,
            cur_amt: None,
            desc: None,
            exp_dt: None,
        }
    }
}
