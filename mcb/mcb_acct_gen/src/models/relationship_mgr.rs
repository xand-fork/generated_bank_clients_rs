/*
 * IFX Services for Metropolitan
 *
 * IFX Services for Metropolitan
 *
 * The version of the OpenAPI document: 0.0.1
 *
 * Generated by: https://openapi-generator.tech
 */

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct RelationshipMgr {
    #[serde(rename = "Desc", skip_serializing_if = "Option::is_none")]
    pub desc: Option<String>,
    #[serde(
        rename = "RelationshipMgrIdent",
        skip_serializing_if = "Option::is_none"
    )]
    pub relationship_mgr_ident: Option<String>,
    #[serde(rename = "RelationshipRole", skip_serializing_if = "Option::is_none")]
    pub relationship_role: Option<String>,
}

impl RelationshipMgr {
    pub fn new() -> RelationshipMgr {
        RelationshipMgr {
            desc: None,
            relationship_mgr_ident: None,
            relationship_role: None,
        }
    }
}
