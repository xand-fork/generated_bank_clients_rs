/*
 * IFX Services for Metropolitan
 *
 * IFX Services for Metropolitan
 *
 * The version of the OpenAPI document: 0.0.1
 *
 * Generated by: https://openapi-generator.tech
 */

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct AcctInfoDtl {
    #[serde(rename = "ConfirmationNbr", skip_serializing_if = "Option::is_none")]
    pub confirmation_nbr: Option<String>,
}

impl AcctInfoDtl {
    pub fn new() -> AcctInfoDtl {
        AcctInfoDtl {
            confirmation_nbr: None,
        }
    }
}
