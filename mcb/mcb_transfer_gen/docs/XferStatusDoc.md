# XferStatusDoc

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**eff_dt** | Option<**String**> |  | [optional]
**xfer_status_code** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


