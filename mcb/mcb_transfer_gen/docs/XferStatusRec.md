# XferStatusRec

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**xfer_keys** | Option<[**crate::models::XferKeys**](XferKeys.md)> |  | [optional]
**xfer_status** | Option<[**crate::models::XferStatusDoc**](XferStatusDoc.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


