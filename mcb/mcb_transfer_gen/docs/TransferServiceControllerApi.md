# \TransferServiceControllerApi

All URIs are relative to *http://localhost:8091/efxService/v0_1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**xfer_add_using_post**](TransferServiceControllerApi.md#xfer_add_using_post) | **POST** /Transfers | xferAdd



## xfer_add_using_post

> crate::models::XferRes xfer_add_using_post(xfer_req)
xferAdd

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**xfer_req** | [**XferReq**](XferReq.md) | XferReq | [required] |

### Return type

[**crate::models::XferRes**](XferRes.md)

### Authorization

[mcb_auth](../README.md#mcb_auth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

