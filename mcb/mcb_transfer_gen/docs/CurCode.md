# CurCode

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cur_code_type** | Option<**String**> |  | [optional]
**cur_code_value** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


