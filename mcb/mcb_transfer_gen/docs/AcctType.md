# AcctType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**acct_type_source** | Option<**String**> |  | [optional]
**acct_type_value** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


