# Status

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ovrd_exception_ind** | Option<**String**> |  | [optional]
**server_status_code** | Option<**String**> |  | [optional]
**server_status_desc** | Option<**String**> |  | [optional]
**severity** | Option<**String**> |  | [optional]
**status_code** | Option<**String**> |  | [optional]
**status_desc** | Option<**String**> |  | [optional]
**svc_provider_name** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


