# XferResXferRes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**efx_hdr** | Option<[**crate::models::EfxHdr**](EFXHdr.md)> |  | [optional]
**efx_status** | Option<[**crate::models::Status**](Status.md)> |  | [optional]
**xfer_status** | Option<[**crate::models::XferStatus**](XferStatus.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


